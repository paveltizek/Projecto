package cz.tizek.database;


import com.google.common.base.CaseFormat;
import com.google.common.base.Strings;
import cz.tizek.model.Address;
import cz.tizek.model.Model;
import cz.tizek.model.Role;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pavel on 05.11.2015.
 */
public class Wrapper<T extends Model> {
    protected int id;
    private T model;
    private ArrayList<String> attrArrayList;

    public Wrapper(T model) {

        this.model = model;
        attrArrayList = new ArrayList<String>();
    }

    public Wrapper() {
        attrArrayList = new ArrayList<String>();
    }

    public Wrapper(int id) {
        this.id = id;
    }

    public T getModel() {
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {

            String sql = "SELECT * FROM " + this.model.getClass().getSimpleName() + " WHERE " + this.model.getClass().getSimpleName() + "_id = ?";
            System.out.println(sql);
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.clearParameters();
            preparedStatement.setInt(1, this.model.getId());
            ResultSet resultSet = preparedStatement.executeQuery();

            Method[] allMethods = this.model.getClass().getMethods();
            List<Method> setters = new ArrayList<Method>();
            while (resultSet.next()) {
                for (Method method : allMethods) {
                    if (method.getName().equals("setId")) {
                        String columnName =
                                this.model.getClass().getSimpleName() +
                                        "_" +
                                        CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, (method.getName()).substring(3));
                        method.invoke(this.model, resultSet.getObject(columnName));
                        continue;
                    }
                    if (method.getName().startsWith("set")) {
                        setters.add(method);
                        String columnName = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, (method.getName()).substring(3));
                        Class<?> argType = method.getParameters()[0].getType();

                        if (Model.class.isAssignableFrom(argType)) {
                            columnName += "_id";
                            Constructor constructor = argType.getConstructor(new Class[]{Integer.class});
                            method.invoke(this.model, constructor.newInstance(resultSet.getObject(columnName)));
                        } else {
                            method.invoke(this.model, resultSet.getObject(columnName));
                        }
                    }
                }
            }

            resultSet.close();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        database.close();

        return (T) model;
    }

    public boolean insert() throws NullPointerException {
        System.out.println("insert");
        Field[] fields = this.model.getClass().getDeclaredFields();
        for (Field field : fields) {
            try {
                field.setAccessible(true);
                if (field.get(this.model) == null) {
                    throw new NullPointerException();
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();
        PreparedStatement preparedStatement;

        System.out.println(this.attrArrayList);

        String[] attributes = this.getAttributes(this.model);

        try {

            String sql = "INSERT INTO " + this.model.getClass().getSimpleName() + " (" + attributes[0] + ") VALUES (" + attributes[3] + ")";
            System.out.println("Insert: " + sql);
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.clearParameters();
//

            Method[] allMethods = this.model.getClass().getMethods();
            List<Method> getters = new ArrayList<Method>();


            int i = 1;
            for (String attribute : this.attrArrayList) {
                System.out.println(CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_CAMEL, (attribute)));
                for (Method method : allMethods) {

                    if (method.getName().startsWith("get")
                            && !method.getName().equals("getId")
                            && !method.getName().equals("getClass")
                            && !method.getName().equals("getObject")
                            && method.getName().substring(3).equals(CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_CAMEL, (attribute)))) {
                        getters.add(method);
//                    System.out.println("Method: " + method);
//                        System.out.println("metoda: " + method.getName().substring(3));
//                    String methodName = "get" + method.getName().substring(3);

                        Object object;
//                        System.out.println(Model.class.isAssignableFrom(method.getReturnType()));
                        if (Model.class.isAssignableFrom(method.getReturnType())) {
                            object = method.invoke(this.model);
                            Model model = (Model) object;
                            Integer id = model.getId();
                            object = id;
                        } else {
                            object = method.invoke(this.model);
                        }
                        System.out.println("Object: " + method.getName() + ", " + object);

                        preparedStatement.setObject(i++, object);

                        String columnName = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, (method.getName()).substring(3));
//                        method.invoke(this.model, resultSet.getObject(columnName));

                    }

                }
            }



//            System.out.println(getters);
                /*for (Method getter : getters) {
                    String attrName = CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, (getter.getName()).substring(3));
                    System.out.println("atrtr: " + attrName);
                }*/


//            }

            preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }


        database.close();

        return true;
    }

    public boolean update() {
        System.out.println("insert");
        Field[] fields = this.model.getClass().getDeclaredFields();
        for (Field field : fields) {
            try {
                field.setAccessible(true);
                if (field.get(this.model) == null) {
                    throw new NullPointerException();
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();
        PreparedStatement preparedStatement;

        System.out.println(this.attrArrayList);

        String[] attributes = this.getAttributes(this.model);

        try {

            String sql = "UPDATE " + this.model.getClass().getSimpleName() + " SET " + attributes[4] + " WHERE " + attributes[1] + "=?";
            System.out.println("Update: " + sql);
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.clearParameters();
//

            Method[] allMethods = this.model.getClass().getMethods();
            List<Method> getters = new ArrayList<Method>();


            preparedStatement.setInt(Integer.parseInt(attributes[2]), this.model.getId());
            int i = 1;
            for (String attribute : this.attrArrayList) {
                System.out.println(CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_CAMEL, (attribute)));
                for (Method method : allMethods) {

                    if (method.getName().startsWith("get")
                            && !method.getName().equals("getId")
                            && !method.getName().equals("getClass")
                            && !method.getName().equals("getObject")
                            && method.getName().substring(3).equals(CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_CAMEL, (attribute)))) {
                        getters.add(method);
//                    System.out.println("Method: " + method);
//                        System.out.println("metoda: " + method.getName().substring(3));
//                    String methodName = "get" + method.getName().substring(3);

                        Object object;
//                        System.out.println(Model.class.isAssignableFrom(method.getReturnType()));
                        if (Model.class.isAssignableFrom(method.getReturnType())) {
                            object = method.invoke(this.model);
                            Model model = (Model) object;
                            Integer id = model.getId();
                            object = id;
                        } else {
                            object = method.invoke(this.model);
                        }
                        System.out.println("Object: " + method.getName() + ", " + object);

                        preparedStatement.setObject(i++, object);

                        String columnName = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, (method.getName()).substring(3));
//                        method.invoke(this.model, resultSet.getObject(columnName));

                    }

                }
            }



//            System.out.println(getters);
                /*for (Method getter : getters) {
                    String attrName = CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, (getter.getName()).substring(3));
                    System.out.println("atrtr: " + attrName);
                }*/


//            }

            preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }


        database.close();

        return true;
    }

    public ArrayList<T> getAll(){
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();
        ArrayList<T> modelArrayList = new ArrayList<T>();

        try {

            String sql = "SELECT * FROM " + this.model.getClass().getSimpleName();
            System.out.println(sql);
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.clearParameters();
//            preparedStatement.setInt(1, this.model.getId());
            ResultSet resultSet = preparedStatement.executeQuery();

            Method[] allMethods = this.model.getClass().getMethods();
            List<Method> setters = new ArrayList<Method>();
            while (resultSet.next()) {
                for (Method method : allMethods) {
                    if (method.getName().equals("setId")) {
                        String columnName =
                                this.model.getClass().getSimpleName() +
                                        "_" +
                                        CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, (method.getName()).substring(3));
                        method.invoke(this.model, resultSet.getObject(columnName));
                        continue;
                    }
                    if (method.getName().startsWith("set")) {
                        setters.add(method);
                        String columnName = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, (method.getName()).substring(3));
                        Class<?> argType = method.getParameters()[0].getType();

                        if (Model.class.isAssignableFrom(argType)) {
                            columnName += "_id";
                            Constructor constructor = argType.getConstructor(new Class[]{Integer.class});
                            method.invoke(this.model, constructor.newInstance(resultSet.getObject(columnName)));
//                            Constructor constructor = argType.getConstructor(new Class[]{argType});
//                            method.invoke(this.model, constructor.newInstance(resultSet.getObject(columnName)));
                        } else {
                            method.invoke(this.model, resultSet.getObject(columnName));
                        }

                    }
                }
                modelArrayList.add(this.model);
            }

            resultSet.close();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        database.close();
        return modelArrayList;
    }

    public String[] getAttributes(T object) {
//        Field idField = object.getClass().getSuperclass().getDeclaredField("id");


        int fieldsCount = 0;
        String questionsString = "";
        String idField = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, (object.getClass().getSimpleName() + "_id"));
        String fieldsString = "";
        String updateString = "";
        ++fieldsCount;
        Field[] fields = object.getClass().getDeclaredFields();

//            fields.setAccessible(true);
//            Object value = field.get(this.model);

        boolean first = true;
        for (Field field : fields) {
            if (!field.getName().contains("id")) {
//                System.out.println("Field: " + field.getName());
                this.attrArrayList.add(field.getName());
                if (first) {
                    fieldsString += field.getName();
                    updateString += field.getName() + "=?";
                    questionsString += "?";
                    fieldsCount++;
                    first = false;
                } else {
//                    System.out.println("Field: " + field.getName());
//                    System.out.println("Type: " + field.getType().getSimpleName().toString().toLowerCase());
//                    System.out.println(field.getName().equals(field.getType().getSimpleName().toString().toLowerCase()));

                    if (field.getName().equals(field.getType().getSimpleName().toString().toLowerCase())) {
                        fieldsString += ", " + field.getName() + "_id";
                        updateString += ", " + field.getName() + "_id=?";
//                        this.attrArrayList.add(field.getName() + "_id");
                        questionsString += ", ?";
                        fieldsCount++;

                    } else {
                        fieldsString += ", " + field.getName();
                        updateString += ", " + field.getName() + "=?";
                        questionsString += ", ?";
                        fieldsCount++;
                    }


                }


            }

        }

//        for (String x : attrArrayList){
//            System.out.println("x: " + x);
//        }
        fieldsString = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, (fieldsString));
        updateString = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, (updateString));
//        System.out.println(fieldsString);
//        System.out.println(questionsString);
//        System.out.println();
//        System.out.println("fieldsString: " + fieldsString);
//        System.out.println("questionsString: " + questionsString);
        return new String[]{fieldsString, idField, String.valueOf(fieldsCount), questionsString, updateString};
    }
}
