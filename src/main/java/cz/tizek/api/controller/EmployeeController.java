package cz.tizek.api.controller;


import com.google.gson.Gson;
import cz.tizek.model.Employee;
import cz.tizek.model.Role;
import org.json.simple.JSONObject;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

/**
 * Created by Pavel on 02.11.2015.
 */

@Path("/user")
public class EmployeeController extends BaseController {




    @GET
    @Produces("application/json")
    public Response getUser(){
        new EmployeeController();
        ArrayList<Employee> employees = new Employee().get();
        Gson gson = new Gson();
        String json = gson.toJson(employees);

        return Response.status(200)
                .header("Access-Control-Allow-Origin", "")
                .entity(json)
                .build();
    }

    @GET
    @Produces("application/json")
    @Path("{arg}")
    public Response getUser(@PathParam("arg") Integer arg){
        JSONObject jsonObject = new JSONObject();
        Employee employee = new Employee(arg);
        jsonObject.put("name", employee.getFirstName());

        return Response.status(200).entity(jsonObject.toJSONString()).build();
    }


}
