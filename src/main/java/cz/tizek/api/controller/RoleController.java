package cz.tizek.api.controller;

import com.google.gson.Gson;
import cz.tizek.model.Role;
import org.json.simple.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

/**
 * Created by Pavel on 05.11.2015.
 */
@Path("/role")
public class RoleController extends BaseController {

    @GET
    @Produces("application/json")
    public Response getRoles(){

        JSONObject jsonObject = new JSONObject();
        ArrayList<Role> roles = new Role().get();
        Gson gson = new Gson();
        String json = gson.toJson(roles);

        return Response.status(200)
                .header("Access-Control-Allow-Origin", "")
                .entity(json)
                .build();

    }

    @Path("{id : \\d+}")
    @GET
    @Produces("application/json")
    public Response getRole(@Context HttpServletRequest request, @PathParam("id") String id){

        Role role = new Role(Integer.valueOf(id));
        /*JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", role.getRoleName());
        jsonObject.put("salary", role.getSalary());*/
        Gson gson = new Gson();
        String json = gson.toJson(role);
        HttpSession session = request.getSession();
        session.setAttribute("test", json);
        return Response.status(200).entity(json).build();

    }

    @POST
    @Produces("application/json")
    public Response store(
            @FormParam("role_name") String roleName,
            @FormParam("salary") String salary
    ){


        System.out.println(roleName);
        System.out.println(salary);
        Role role = new Role(roleName, Double.valueOf(salary));
        /*JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", role.getRoleName());
        jsonObject.put("salary", role.getSalary());
        */
        role.save();
        Gson gson = new Gson();
        String json = gson.toJson(role);

        return Response.status(200).entity(json).build();

    }

    @Path("{id : \\d+}")
    @POST
    @Produces("application/json")
    public Response update(
            @PathParam("id") String id,
            @FormParam("role_name") String roleName,
            @FormParam("salary") String salary
    ){
        System.out.println(roleName);

        Role role = new Role(Integer.valueOf(id));
        if (!roleName.equals("")){
            role.setRoleName(roleName);
        }
        if (!salary.equals("")){
            role.setSalary(Double.valueOf(salary));
        }

        role.save();

        /*JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", role.getRoleName());
        jsonObject.put("salary", role.getSalary());
        */
        Gson gson = new Gson();
        String json = gson.toJson(id);

        return Response.status(200).build();

    }
}
