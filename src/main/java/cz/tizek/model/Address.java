package cz.tizek.model;

/**
 * Created by Pavel on 07.11.2015.
 */
public class Address extends Model {

    private String street;
    private String postCode;
    private String country;
    private String city;

    public Address() {
        super();
    }

    public Address(Integer id) {
        super(id);
    }

    public Address(Integer id, String street, String postCode, String city, String country) {
        this.id = id;
        this.street = street;
        this.postCode = postCode;
        this.city = city;
        this.country = country;
    }

    public Address(String street, String postCode, String city, String country) {
        this.street = street;
        this.postCode = postCode;
        this.city = city;
        this.country = country;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public void save() {
        super.save();
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", street='" + street + '\'' +
                ", postCode='" + postCode + '\'' +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
