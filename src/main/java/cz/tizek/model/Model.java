package cz.tizek.model;

import cz.tizek.database.Wrapper;

import java.util.ArrayList;

/**
 * Created by Pavel on 05.11.2015.
 */
public abstract class Model<T> {
    protected Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Model() {

        this.id = 0;

    }

    public Model(int id) {
        this.id = id;
        this.getObject();
    }

    public T getObject() {
        Wrapper wrapper = new Wrapper(this);
        return (T) wrapper.getModel();

    }

    public void save(){
        Wrapper wrapper = new Wrapper(this);

        try {
            if (this.id == 0){
                wrapper.insert();
            }
            else {
                wrapper.update();
            }
        } catch (NullPointerException e) {
            System.out.println("Object cannot be saved, you must fill all attributes.");
            e.printStackTrace();
        }
    }

    public ArrayList<T> get(){
        return new Wrapper(this).getAll();
    }



}
