package cz.tizek.model;

import cz.tizek.database.Wrapper;

/**
 * Created by Pavel on 05.11.2015.
 */
public class Role extends Model {


    private String roleName;
    private Double salary;

    public Role() {
        super();
    }

    public Role(Integer id) {
        super(id);
//        System.out.println(new Model<Role>(id).getObject());


    }

    public Role(String roleName, double salary) {
        this.roleName = roleName;
        this.salary = salary;
    }

    public Role(Integer id, String roleName, double salary) {

        this.id = id;
        this.roleName = roleName;
        this.salary = salary;
    }



    public String getRoleName() {

        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public void save() {
        super.save();

    }

    @Override
    public String toString() {
        return "Role{" +
                "roleName='" + roleName + '\'' +
                ", salary=" + salary +
                '}';
    }
}
