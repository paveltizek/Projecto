package Database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Pavel on 26.04.2015.
 */
public class Notification {
    private int id;
    private String message;
    private Employee employee;

    public Notification() {
        this.id = 0;
    }

    public Notification(int id) {
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM NOTIFICATION WHERE NOTIFICATION_ID=?");
            preparedStatement.clearParameters();
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {

                this.id = resultSet.getInt("notification_id");
                this.message = resultSet.getString("message");
                this.employee = new Employee(resultSet.getInt("employee_id"));


            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
    }

    public Notification(String message, Employee employee) {
        this.id = 0;
        this.message = message;
        this.employee = employee;
    }

    public Notification(int id, String message, Employee employee) {
        this.id = id;
        this.message = message;
        this.employee = employee;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public int getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public Employee getEmployee() {
        return employee;
    }

    public ArrayList<Notification> all(){
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM NOTIFICATION");
            preparedStatement.clearParameters();
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Notification> notifications = new ArrayList<>();
            while (resultSet.next()) {
                Notification notification = new Notification();
                notification.id = resultSet.getInt("notofication_id");
                notification.message = resultSet.getString("message");
                notification.employee = new Employee(resultSet.getInt("employee_id"));
                notifications.add(notification);
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
            return notifications;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
        return null;
    }

    public ArrayList<Notification> getByEmployee(Employee employee){
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM NOTIFICATION WHERE EMPLOYEE_ID=?");
            preparedStatement.clearParameters();
            preparedStatement.setInt(1, employee.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Notification> notifications = new ArrayList<>();
            while (resultSet.next()) {
                Notification notification = new Notification();
                notification.id = resultSet.getInt("notification_id");
                notification.message = resultSet.getString("message");
                notification.employee = new Employee(resultSet.getInt("employee_id"));
                notifications.add(notification);
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
            return notifications;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
        return null;
    }

    public ArrayList<Notification> getByEmployee(String lastName){
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM NOTIFICATION WHERE EMPLOYEE_ID=?");
            preparedStatement.clearParameters();
            preparedStatement.setString(1, "%" + lastName + "%");
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Notification> notifications = new ArrayList<>();
            while (resultSet.next()) {
                Notification notification = new Notification();
                notification.id = resultSet.getInt("notification_id");
                notification.message = resultSet.getString("message");
                notification.employee = new Employee(resultSet.getInt("employee_id"));
                notifications.add(notification);
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
            return notifications;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
        return null;
    }

    public boolean save(){
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement;
            if (this.id == 0) {
                preparedStatement = connection.prepareStatement("INSERT INTO NOTIFICATION (MESSAGE, EMPLOYEE_ID) VALUES (?, ?)");
                preparedStatement.clearParameters();
                preparedStatement.setString(1, this.message);
                preparedStatement.setInt(2, this.employee.getId());
            } else {
                preparedStatement = connection.prepareStatement("UPDATE NOTIFICATION SET MESSAGE=?, EMPLOYEE_ID=? WHERE NOTIFICATION_ID=?");
                preparedStatement.clearParameters();
                preparedStatement.setString(1, this.message);
                preparedStatement.setInt(2, this.employee.getId());
                preparedStatement.setInt(3, this.id);
            }

            preparedStatement.execute();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        database.close();
        return true;
    }

    @Override
    public String toString() {
        return "Notification{" +
                "id=" + id +
                ", message='" + message + '\'' +
                ", employee=" + employee +
                '}';
    }
}
