package Database;

import Helpers.StringComparator;
import java.sql.*;
import java.util.ArrayList;


public class Employee {
    private int id;
    private String firstName;
    private String lastName;
    private String password;
    private String email;
    private Date hireDate;
    private Date endDate;
    private Role role;
    private Address address;

    public Employee() {
        this.id = 0;
    }

    public Employee(int id) {
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM EMPLOYEE WHERE employee_id=?");
            preparedStatement.clearParameters();
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {

                this.id = resultSet.getInt("employee_id");
                this.firstName = resultSet.getString("first_name");
                this.lastName = resultSet.getString("last_name");
                this.password = resultSet.getString("password");
                this.email = resultSet.getString("email");
                this.hireDate = resultSet.getDate("hire_date");
                this.endDate = resultSet.getDate("end_date");
                this.role = new Role(resultSet.getInt("role_id"));
                this.address = new Address(resultSet.getInt("address_id"));


            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
    }

    public Employee(String firstName, String lastName, String password, String email, Date hireDate, Date endDate, Role role, Address address) {
        this.id = 0;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.email = email;
        this.hireDate = hireDate;
        this.endDate = endDate;
        this.role = role;
        this.address = address;
    }

    public Employee(int id, String firstName, String lastName, String password, String email, Date hireDate, Date endDate, Role role, Address address) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.email = email;
        this.hireDate = hireDate;
        this.endDate = endDate;
        this.role = role;
        this.address = address;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setHireDate(Date hireDate) {
        this.hireDate = hireDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public Date getHireDate() {
        return hireDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public Role getRole() {
        return role;
    }

    public Address getAddress() {
        return address;
    }

    public ArrayList<Employee> all() {
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM EMPLOYEE");
            preparedStatement.clearParameters();
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Employee> employees = new ArrayList<>();
            while (resultSet.next()) {
                Employee employee = new Employee();
                employee.id = resultSet.getInt("employee_id");
                employee.firstName = resultSet.getString("first_name");
                employee.lastName = resultSet.getString("last_name");
                employee.password = resultSet.getString("password");
                employee.email = resultSet.getString("email");
                employee.hireDate = resultSet.getDate("hire_date");
                employee.endDate = resultSet.getDate("end_date");
                employee.role = new Role(resultSet.getInt("role_id"));
                employee.address = new Address(resultSet.getInt("address_id"));
                employees.add(employee);
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
            return employees;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
        return null;
    }

    public ArrayList<Employee> getByRole(Role role){
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM EMPLOYEE WHERE ROLE_ID=?");
            preparedStatement.clearParameters();
            preparedStatement.setInt(1, role.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Employee> employees = new ArrayList<>();
            while (resultSet.next()) {
                Employee employee = new Employee();
                employee.id = resultSet.getInt("employee_id");
                employee.firstName = resultSet.getString("first_name");
                employee.lastName = resultSet.getString("last_name");
                employee.password = resultSet.getString("password");
                employee.email = resultSet.getString("email");
                employee.hireDate = resultSet.getDate("hire_date");
                employee.endDate = resultSet.getDate("end_date");
                employee.role = new Role(resultSet.getInt("role_id"));
                employee.address = new Address(resultSet.getInt("address_id"));
                employees.add(employee);
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
            return employees;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
        return null;
    }

    public ArrayList<Employee> getByRole(String name){
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM EMPLOYEE e LEFT JOIN ROLE r ON e.ROLE_ID=r.ROLE_ID WHERE ROLE_NAME LIKE ?");
            preparedStatement.clearParameters();
            preparedStatement.setString(1, "%" + name + "%");
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Employee> employees = new ArrayList<>();
            while (resultSet.next()) {
                Employee employee = new Employee();
                employee.id = resultSet.getInt("employee_id");
                employee.firstName = resultSet.getString("first_name");
                employee.lastName = resultSet.getString("last_name");
                employee.password = resultSet.getString("password");
                employee.email = resultSet.getString("email");
                employee.hireDate = resultSet.getDate("hire_date");
                employee.endDate = resultSet.getDate("end_date");
                employee.role = new Role(resultSet.getInt("role_id"));
                employee.address = new Address(resultSet.getInt("address_id"));
                employees.add(employee);
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
            return employees;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
        return null;
    }

    public boolean save() {
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement;
            if (this.id == 0) {
                preparedStatement = connection.prepareStatement("INSERT INTO EMPLOYEE (FIRST_NAME, LAST_NAME, PASSWORD, EMAIL, HIRE_DATE, END_DATE, ROLE_ID, ADDRESS_ID) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
                preparedStatement.clearParameters();
                preparedStatement.setString(1, this.firstName);
                preparedStatement.setString(2, this.lastName);
                preparedStatement.setString(3, this.password);
                preparedStatement.setString(4, this.email);
                preparedStatement.setDate(5, this.hireDate);
                preparedStatement.setDate(6, this.endDate);
                preparedStatement.setInt(7, this.role.getId());
                preparedStatement.setInt(8, this.address.getId());
            } else {
                System.out.println("Update");
                preparedStatement = connection.prepareStatement("UPDATE EMPLOYEE SET FIRST_NAME=?, LAST_NAME=?, PASSWORD=?, EMAIL=?, HIRE_DATE=?, END_DATE=?, ROLE_ID=?, ADDRESS_ID=? WHERE address_id=?");
                preparedStatement.clearParameters();
                preparedStatement.setString(1, this.firstName);
                preparedStatement.setString(2, this.lastName);
                preparedStatement.setString(3, this.password);
                preparedStatement.setString(4, this.email);
                preparedStatement.setDate(5, this.hireDate);
                preparedStatement.setDate(6, this.endDate);
                preparedStatement.setInt(7, this.role.getId());
                preparedStatement.setInt(8, this.address.getId());
                preparedStatement.setInt(9, this.id);
            }

            preparedStatement.execute();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        database.close();
        return true;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", hireDate=" + hireDate +
                ", endDate=" + endDate +
                ", role=" + role +

                ", address=" + address +
                '}';
    }

    public float getTimeWorked(){
        ArrayList<TicketWork> ticketWorks = new TicketWork().getByEmployee(this);
        float sum = 0;
        for (TicketWork ticketWork : ticketWorks){
            sum += ticketWork.getTime();
        }
        return sum;
    }

    public void createPayout(){
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            CallableStatement callableStatement = connection.prepareCall("{call createPayout(?)}");
            callableStatement.setInt(1, this.id);
            callableStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
