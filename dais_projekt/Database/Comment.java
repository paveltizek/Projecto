package Database;

import Helpers.MyDate;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by Pavel on 26.04.2015.
 */
public class Comment {
    private int id;
    private String text;
    private Date time;
    private Integer parent_id;
    private Ticket ticket;
    private Employee employee;

    public Comment() {
        this.id = 0;
        this.parent_id = null;
    }

    public Comment(int id) {
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM  Comment c LEFT JOIN TICKET t ON WHERE COMMENT_ID=?");
            preparedStatement.clearParameters();
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {

                this.id = resultSet.getInt("project_id");
                this.text = resultSet.getString("name");
                this.time = MyDate.toDate(resultSet.getString("time"));
                this.parent_id = resultSet.getInt("parent_id");
                this.ticket = new Ticket(resultSet.getInt("ticket_id"));
                this.employee = new Employee(resultSet.getInt("employee_id"));


            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
    }

    public Comment(String text, Date time, Integer parent_id, Ticket ticket, Employee employee) {
        this.id = 0;
        this.text = text;
        this.time = time;
        this.parent_id = parent_id;
        this.ticket = ticket;
        this.employee = employee;
    }

    public Comment(int id, String text, Date time, Integer parent_id, Ticket ticket, Employee employee) {
        this.id = id;
        this.text = text;
        this.time = time;
        this.parent_id = parent_id;
        this.ticket = ticket;
        this.employee = employee;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public int getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public Date getTime() {
        return time;
    }

    public Integer getParent_id() {
        return parent_id;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public Employee getEmployee() {
        return employee;
    }

    public ArrayList<Comment> getByTicket(Ticket ticket) {
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM Comment WHERE TICKET_ID=?");
            preparedStatement.clearParameters();
            preparedStatement.setInt(1, ticket.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Comment> comments = new ArrayList<>();
            while (resultSet.next()) {
                Comment comment = new Comment();
                comment.id = resultSet.getInt("comment_id");
                comment.text = resultSet.getString("text");
                comment.time = MyDate.toDate(resultSet.getString("time"));
                comment.parent_id = resultSet.getInt("parent_id");
                comment.ticket = new Ticket(resultSet.getInt("ticket_id"));
                comment.employee = new Employee(resultSet.getInt("employee_id"));

                comments.add(comment);
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
            return comments;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
        return null;
    }

    public ArrayList<Comment> getByEmployee(Employee employee) {
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM Comment WHERE EMPLOYEE_ID=?");
            preparedStatement.clearParameters();
            preparedStatement.setInt(1, employee.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Comment> comments = new ArrayList<>();
            while (resultSet.next()) {
                Comment comment = new Comment();
                comment.id = resultSet.getInt("comment_id");
                comment.text = resultSet.getString("text");
                comment.time = MyDate.toDate(resultSet.getString("time"));
                comment.parent_id = resultSet.getInt("parent_id");
                comment.ticket = new Ticket(resultSet.getInt("ticket_id"));
                comment.employee = new Employee(resultSet.getInt("employee_id"));

                comments.add(comment);
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
            return comments;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
        return null;
    }

    public ArrayList<Comment> all(){

        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM Comment");
            preparedStatement.clearParameters();
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Comment> comments = new ArrayList<>();
            while (resultSet.next()) {
                Comment comment = new Comment();
                comment.id = resultSet.getInt("comment_id");
                comment.text = resultSet.getString("text");
                comment.time = MyDate.toDate(resultSet.getString("time"));
                comment.parent_id = resultSet.getInt("parent_id");
                comment.ticket = new Ticket(resultSet.getInt("ticket_id"));
                comment.employee = new Employee(resultSet.getInt("employee_id"));

                comments.add(comment);
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
            return comments;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
        return null;
    }

    public boolean save(){

        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement;
            if (this.id == 0) {
                preparedStatement = connection.prepareStatement("INSERT INTO Comment (TEXT, TIME , PARRENT_ID, TICKET_ID, EMPLOYEE_ID) VALUES (?, ?, ?, ?, ?)");
                preparedStatement.clearParameters();
                preparedStatement.setString(1, this.text);
                preparedStatement.setDate(2, this.time);
                preparedStatement.setInt(3, this.parent_id);
                preparedStatement.setInt(4, this.ticket.getId());
                preparedStatement.setInt(5, this.employee.getId());


                System.out.println(this);
            } else {
                preparedStatement = connection.prepareStatement("UPDATE Comment SET TEXT=?, TIME =?, PARRENT_ID=?, TICKET_ID=?, EMPLOYEE_ID=? WHERE COMMENT_ID=?");
                preparedStatement.clearParameters();
                preparedStatement.setString(1, this.text);
                preparedStatement.setDate(2, this.time);
                preparedStatement.setInt(3, this.parent_id);
                preparedStatement.setInt(4, this.ticket.getId());
                preparedStatement.setInt(5, this.employee.getId());
                preparedStatement.setInt(6, this.id);
            }

            preparedStatement.execute();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        database.close();
        return true;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", time=" + time +
                ", parent_id=" + parent_id +
                ", ticket=" + ticket +
                ", employee=" + employee +
                '}';
    }
}
