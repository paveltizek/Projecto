package Database;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by Pavel on 25.04.2015.
 */
public class TicketWork {
    private int id;
    private String text;
    private float time;
    private Date timestamp;
    private Employee employee;
    private Ticket ticket;

    public TicketWork() {
        this.id = 0;
    }

    public TicketWork(int id) {

        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM TICKETWORK WHERE WORK_ID=?");
            preparedStatement.clearParameters();
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {

                this.id = resultSet.getInt("work_id");
                this.text = resultSet.getString("text");
                this.time = resultSet.getFloat("time");
                this.timestamp = resultSet.getDate("timestamp");
                this.employee = new Employee(resultSet.getInt("employee_id"));
                this.ticket = new Ticket(resultSet.getInt("ticket_id"));


            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
    }

    public TicketWork(String text, float time, Date timestamp, Employee employee, Ticket ticket) {
        this.text = text;
        this.time = time;
        this.timestamp = timestamp;
        this.employee = employee;
        this.ticket = ticket;
    }

    public TicketWork(int id, String text, float time, Date timestamp, Employee employee, Ticket ticket) {
        this.id = id;
        this.text = text;
        this.time = time;
        this.timestamp = timestamp;
        this.employee = employee;
        this.ticket = ticket;
    }

    public int getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public float getTime() {
        return time;
    }

    public Employee getEmployee() {
        return employee;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setTime(float time) {
        this.time = time;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public ArrayList<TicketWork> all() {
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM TICKETWORK");
            preparedStatement.clearParameters();
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<TicketWork> ticketWorks = new ArrayList<>();
            while (resultSet.next()) {
                TicketWork ticketWork = new TicketWork();
                ticketWork.id = resultSet.getInt("invoice_id");
                ticketWork.text = resultSet.getString("text");
                ticketWork.time = resultSet.getFloat("time");
                ticketWork.ticket = new Ticket(resultSet.getInt("ticket_id"));
                ticketWork.employee = new Employee(resultSet.getInt("employee_id"));
                ticketWorks.add(ticketWork);
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
            return ticketWorks;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
        return null;
    }

    public ArrayList<TicketWork> getByTicket(Ticket ticket) {
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM TICKETWORK WHERE TICKET_ID=?");
            preparedStatement.clearParameters();
            preparedStatement.setInt(1, ticket.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<TicketWork> ticketWorks = new ArrayList<>();
            while (resultSet.next()) {
                TicketWork ticketWork = new TicketWork();
                ticketWork.id = resultSet.getInt("work_id");
                ticketWork.text = resultSet.getString("text");
                ticketWork.time = resultSet.getFloat("time");
                ticketWork.timestamp = resultSet.getDate("timestamp");
                ticketWork.ticket = new Ticket(resultSet.getInt("ticket_id"));
                ticketWork.employee = new Employee(resultSet.getInt("employee_id"));
                ticketWorks.add(ticketWork);
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
            return ticketWorks;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
        return null;
    }

    public ArrayList<TicketWork> getByEmployee(Employee employee) {
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM TICKETWORK WHERE EMPLOYEE_ID=?");
            preparedStatement.clearParameters();
            preparedStatement.setInt(1, employee.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<TicketWork> ticketWorks = new ArrayList<>();
            while (resultSet.next()) {
                TicketWork ticketWork = new TicketWork();
                ticketWork.id = resultSet.getInt("work_id");
                ticketWork.text = resultSet.getString("text");
                ticketWork.time = resultSet.getInt("time");
                ticketWork.timestamp = resultSet.getDate("timestamp");
                ticketWork.ticket = new Ticket(resultSet.getInt("ticket_id"));
                ticketWork.employee = new Employee(resultSet.getInt("employee_id"));
                ticketWorks.add(ticketWork);
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
            return ticketWorks;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
        return null;
    }

    public boolean save() {
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement;
            if (this.id == 0) {
                preparedStatement = connection.prepareStatement("INSERT INTO TICKETWORK (TEXT, TIME, TIMESTAMP, EMPLOYEE_ID, TICKET_ID) VALUES (?, ?, ?, ?, ?)");
                preparedStatement.clearParameters();
                preparedStatement.setString(1, this.text);
                preparedStatement.setFloat(2, this.time);
                preparedStatement.setDate(3, this.timestamp);
                preparedStatement.setInt(4, this.employee.getId());
                preparedStatement.setInt(5, this.ticket.getId());
            } else {
                preparedStatement = connection.prepareStatement("UPDATE TICKETWORK SET TEXT=?, TIME=?, TIMESTAMP=?, EMPLOYEE_ID=?, TICKET_ID=? WHERE WORK_ID=?");
                preparedStatement.clearParameters();
                preparedStatement.setString(1, this.text);
                preparedStatement.setFloat(2, this.time);
                preparedStatement.setDate(3, this.timestamp);
                preparedStatement.setInt(4, this.employee.getId());
                preparedStatement.setInt(5, this.ticket.getId());
                preparedStatement.setInt(6, this.id);
            }

            preparedStatement.execute();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        database.close();
        return true;
    }

    @Override
    public String toString() {
        return "TicketWork{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", time=" + time +
                ", timestamp=" + timestamp +
                ", employee=" + employee +
                ", ticket=" + ticket +
                '}';
    }
}
