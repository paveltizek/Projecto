package Database;

import Helpers.StringComparator;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Date;


public class Ticket {
    private int id;
    private String name;
    private String detail;
    private String status;
    private float time;
    private Date timestamp;
    private Project project;

    public Ticket() {
        this.id = 0;
    }

    public Ticket(int id) {
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM TICKET WHERE TICKET_ID=?");
            preparedStatement.clearParameters();
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {

                this.id = resultSet.getInt("ticket_id");
                this.name = resultSet.getString("name");
                this.detail = resultSet.getString("detail");
                this.status = resultSet.getString("status");
                this.time = resultSet.getFloat("time");
                this.timestamp = resultSet.getDate("timestamp");
                this.project = new Project(resultSet.getInt("project_id"));


            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
    }

    public Ticket(String name, String detail, String status, float time, Date timestamp, Project project) {
        this.id = 0;
        this.name = name;
        this.detail = detail;
        this.status = status;
        this.time = time;
        this.timestamp = timestamp;
        this.project = project;
    }

    public Ticket(int id, String name, String detail, String status, float time, Date timestamp, Project project) {
        this.id = id;
        this.name = name;
        this.detail = detail;
        this.status = status;
        this.time = time;
        this.timestamp = timestamp;
        this.project = project;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }


    public void setStatus(String status) {
        this.status = status;
    }

    public void setTime(float time) {
        this.time = time;
    }


    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDetail() {
        return detail;
    }

    public String getStatus() {
        return status;
    }

    public float getTime() {
        return time;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public Project getProject() {
        return project;
    }

    public ArrayList<Ticket> all() {
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM TICKET");
            preparedStatement.clearParameters();
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Ticket> tickets = new ArrayList<>();
            while (resultSet.next()) {
                Ticket ticket = new Ticket();
                ticket.id = resultSet.getInt("ticket_id");
                ticket.name = resultSet.getString("name");
                ticket.detail = resultSet.getString("detail");
                ticket.status = resultSet.getString("status");
                ticket.time = resultSet.getFloat("time");
                ticket.timestamp = resultSet.getDate("timestamp");
                ticket.project = new Project(resultSet.getInt("project_id"));
                tickets.add(ticket);
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
            return tickets;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
        return null;
    }

    public ArrayList<Ticket> getByProject(Project project){
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM TICKET WHERE PROJECT_ID=?");
            preparedStatement.clearParameters();
            preparedStatement.setInt(1, this.project.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Ticket> tickets = new ArrayList<>();
            while (resultSet.next()) {
                Ticket ticket = new Ticket();
                ticket.id = resultSet.getInt("ticket_id");
                ticket.name = resultSet.getString("name");
                ticket.detail = resultSet.getString("detail");
                ticket.status = resultSet.getString("status");
                ticket.time = resultSet.getFloat("time");
                ticket.timestamp = resultSet.getDate("timestamp");
                ticket.project = new Project(resultSet.getInt("project_id"));
                tickets.add(ticket);
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
            return tickets;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
        return null;
    }

    public boolean save() {
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement;
            if (this.id == 0) {
                preparedStatement = connection.prepareStatement("INSERT INTO TICKET (NAME, DETAIL, STATUS, TIME, TIMESTAMP, PROJECT_ID) VALUES (?, ?, ?, ?, ?, ?)");
                preparedStatement.clearParameters();
                preparedStatement.setString(1, this.name);
                preparedStatement.setString(2, this.detail);
                preparedStatement.setString(3, this.status);
                preparedStatement.setFloat(4, this.time);
                preparedStatement.setDate(5, this.timestamp);
                preparedStatement.setInt(6, this.project.getId());
            } else {
                System.out.println("Update");
                preparedStatement = connection.prepareStatement("UPDATE TICKET SET NAME=?, DETAIL=?, STATUS=?, TIME=?, TIMESTAMP=?, PROJECT_ID=? WHERE TICKET_ID=?");
                preparedStatement.clearParameters();
                preparedStatement.setString(1, this.name);
                preparedStatement.setString(2, this.detail);
                preparedStatement.setString(3, this.status);
                preparedStatement.setFloat(4, this.time);
                preparedStatement.setDate(5, this.timestamp);
                preparedStatement.setInt(6, this.project.getId());
                preparedStatement.setInt(7, this.id);
            }

            preparedStatement.execute();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        database.close();
        return true;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", detail='" + detail + '\'' +
                ", status='" + status + '\'' +
                ", time=" + time +
                ", timestamp=" + timestamp +
                ", project=" + project +
                '}';
    }


}
