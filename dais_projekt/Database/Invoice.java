package Database;

import Helpers.MyDate;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Pavel on 23.04.2015.
 */
public class Invoice {
    private int id;
    private float amount;
    private String paid;
    private Customer customer;
    private Project project;

    public Invoice() {
        this.id = 0;
    }

    public Invoice(int id) {
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM INVOICE WHERE INVOICE_ID=?");
            preparedStatement.clearParameters();
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {

                this.id = resultSet.getInt("invoice_id");
                this.amount = resultSet.getFloat("amount");
                this.paid = resultSet.getString("paid");
                this.customer = new Customer(resultSet.getInt("customer_id"));
                this.project = new Project(resultSet.getInt("project_id"));


            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
    }

    public Invoice(float amount, String paid, Customer customer, Project project) {
        this.id = 0;
        this.amount = amount;
        this.paid = paid;
        this.customer = customer;
        this.project = project;
    }

    public Invoice(int id, float amount, String paid, Customer customer, Project project) {
        this.id = id;
        this.amount = amount;
        this.paid = paid;
        this.customer = customer;
        this.project = project;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public void setPaid(String paid) {
        this.paid = paid;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public int getId() {
        return id;
    }

    public float getAmount() {
        return amount;
    }

    public String getPaid() {
        return paid;
    }

    public Customer getCustomer() {
        return customer;
    }

    public Project getProject() {
        return project;
    }

    public ArrayList<Invoice> all() {
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM INVOICE");
            preparedStatement.clearParameters();
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Invoice> invoices = new ArrayList<>();
            while (resultSet.next()) {
                Invoice invoice = new Invoice();
                invoice.id = resultSet.getInt("invoice_id");
                invoice.amount = resultSet.getFloat("amount");
                invoice.paid = resultSet.getString("paid");
                invoice.customer = new Customer(resultSet.getInt("customer_id"));
                invoice.project = new Project(resultSet.getInt("project_id"));

                invoices.add(invoice);
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
            return invoices;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
        return null;
    }

    public ArrayList<Invoice> getByCustomer(Customer customer){
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM INVOICE WHERE CUSTOMER_ID=?");
            preparedStatement.clearParameters();
            preparedStatement.setInt(1, customer.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Invoice> invoices = new ArrayList<>();
            while (resultSet.next()) {
                Invoice invoice = new Invoice();
                invoice.id = resultSet.getInt("invoice_id");
                invoice.amount = resultSet.getFloat("amount");
                invoice.paid = resultSet.getString("paid");
                invoice.customer = new Customer(resultSet.getInt("customer_id"));
                invoice.project = new Project(resultSet.getInt("project_id"));

                invoices.add(invoice);
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
            return invoices;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
        return null;
    }

    public boolean save(){
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement;
            if (this.id == 0) {
                preparedStatement = connection.prepareStatement("INSERT INTO INVOICE (AMOUNT, PAID, CUSTOMER_ID, PROJECT_ID) VALUES (?, ?, ?, ?)");
                preparedStatement.clearParameters();
                preparedStatement.setFloat(1, this.amount);
                preparedStatement.setString(2, this.paid);
                preparedStatement.setFloat(3, this.customer.getId());
                preparedStatement.setInt(4, this.project.getId());

            } else {
                preparedStatement = connection.prepareStatement("UPDATE INVOICE SET AMOUNT=?, PAID=?, CUSTOMER_ID=?, PROJECT_ID=? WHERE INVOICE_ID=?");
                preparedStatement.clearParameters();
                preparedStatement.setFloat(1, this.amount);
                preparedStatement.setString(2, this.paid);
                preparedStatement.setFloat(3, this.customer.getId());
                preparedStatement.setInt(4, this.project.getId());
                preparedStatement.setInt(5, this.id);

            }

            preparedStatement.executeQuery();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        database.close();
        return true;
    }

    @Override
    public String toString() {
        return "Invoice{" +
                "id=" + id +
                ", amount=" + amount +
                ", paid='" + paid + '\'' +
                ", customer=" + customer +
                ", project=" + project +
                '}';
    }

    public static void createInvoice(Project project, String paid){
        Customer customer = project.getCustomer();
        Invoice invoice = new Invoice(project.getCost(), paid, customer, project);

        System.out.println(invoice);
        invoice.save();
        //System.out.println(invoice);

    }
}
