package Database;

import Helpers.StringComparator;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Pavel on 20.04.2015.
 */

public class Role {
    public int id;
    protected String roleName;
    protected float salary;

    public Role() {
        this.id = 0;
    }

    public Role(int id) {
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM ROLE WHERE role_id=?");
            preparedStatement.clearParameters();
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {

                this.id = resultSet.getInt("role_id");
                this.roleName = resultSet.getString("role_name");
                this.salary = resultSet.getFloat("salary");
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
    }

    public Role(String roleName, float salary) {
        this.id = 0;
        this.roleName = roleName;
        this.salary = salary;
    }

    public Role(int id, String roleName, float salary) {
        this.id = id;
        this.roleName = roleName;
        this.salary = salary;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public String getRoleName() {
        return roleName;
    }

    public float getSalary() {
        return salary;
    }

    public ArrayList<Role> get(String name) {
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM ROLE WHERE role_name LIKE ?");
            preparedStatement.clearParameters();
            preparedStatement.setString(1, "%" + name + "%");
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Role> roles = new ArrayList<>();
            while (resultSet.next()) {
                Role role = new Role();
                role.id = resultSet.getInt("role_id");
                role.roleName = resultSet.getString("role_name");
                role.salary = resultSet.getFloat("salary");
                roles.add(role);
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
            return roles;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
        return null;
    }

    public ArrayList<Role> all() {
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM ROLE");
            preparedStatement.clearParameters();
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Role> roles = new ArrayList<Role>();
            while (resultSet.next()) {
                Role role = new Role();
                role.id = resultSet.getInt("role_id");
                role.roleName = resultSet.getString("role_name");
                role.salary = resultSet.getFloat("salary");
                roles.add(role);
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
            return roles;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
        return null;
    }

    public boolean save() {
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement;
            if (this.id == 0) {
                preparedStatement = connection.prepareStatement("INSERT INTO ROLE (role_name, salary) VALUES (?, ?)");
                preparedStatement.clearParameters();
                preparedStatement.setString(1, roleName);
                preparedStatement.setFloat(2, salary);

            } else {
                preparedStatement = connection.prepareStatement("UPDATE ROLE SET role_name=?, salary=? WHERE role_id=?");
                preparedStatement.clearParameters();
                preparedStatement.setString(1, this.roleName);
                preparedStatement.setFloat(2, this.salary);
                preparedStatement.setInt(3, this.id);

            }
            preparedStatement.executeQuery();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        database.close();
        return true;
    }

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", roleName='" + roleName + '\'' +
                ", salary=" + salary +
                '}';
    }
}
