package Database;

import Helpers.StringComparator;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class Address {
    private int id;
    private String street;
    private String postCode;
    private String city;
    private String country;

    public Address() {
        this.id = 0;
    }

    public Address(int id) {
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM ADDRESS WHERE address_id=?");
            preparedStatement.clearParameters();
            preparedStatement.setString(1, String.valueOf(id));
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {

                this.id = resultSet.getInt("address_id");
                this.street = resultSet.getString("street");
                this.city = resultSet.getString("city");
                this.postCode = resultSet.getString("post_code");
                this.country = resultSet.getString("country");


            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
    }

    public Address(String street, String postCode, String city, String country) {
        this.id = 0;
        this.street = street;
        this.postCode = postCode;
        this.city = city;
        this.country = country;
    }

    public Address(int id, String street, String postCode, String city, String country) {
        this.id = id;
        this.street = street;
        this.postCode = postCode;
        this.city = city;
        this.country = country;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }


    public void setCity(String city) {
        this.city = city;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getId() {
        return id;
    }

    public String getStreet() {
        return street;
    }

    public String getPostCode() {
        return postCode;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

       public ArrayList<Address> all() {
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM ADDRESS");
            preparedStatement.clearParameters();
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Address> addresses = new ArrayList<>();
            while (resultSet.next()) {
                Address address = new Address();
                address.id = resultSet.getInt("address_id");
                address.street = resultSet.getString("street");
                address.city = resultSet.getString("city");
                address.postCode = resultSet.getString("post_code");
                address.country = resultSet.getString("country");
                addresses.add(address);
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
            return addresses;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
        return null;
    }

    public boolean save() {
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement;
            if (this.id == 0) {
                preparedStatement = connection.prepareStatement("INSERT INTO Address (street, post_code, city, country) VALUES (?, ?, ?, ?)");
                preparedStatement.clearParameters();
                preparedStatement.setString(1, this.street);
                preparedStatement.setString(2, this.postCode);
                preparedStatement.setString(3, this.city);
                preparedStatement.setString(4, this.country);
            } else {
                preparedStatement = connection.prepareStatement("UPDATE ADDRESS SET street=?, post_code=?, city=?, country=? WHERE address_id=?");
                preparedStatement.clearParameters();
                preparedStatement.setString(1, this.street);
                preparedStatement.setString(2, this.postCode);
                preparedStatement.setString(3, this.city);
                preparedStatement.setString(4, this.country);
                preparedStatement.setInt(5, this.id);
            }

            preparedStatement.execute();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        database.close();
        return true;
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", street='" + street + '\'' +
                ", postCode='" + postCode + '\'' +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
