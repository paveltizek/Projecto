package Database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Pavel on 20.04.2015.
 */
public class Database {
    private Connection connection;

    public void open(){
        try {

            String user = "";
            String password = "";
            this.connection = DriverManager.getConnection("jdbc:oracle:thin:@dbedu.cs.vsb.cz:1521:oracle", user, password);
            Class.forName("oracle.jdbc.OracleDriver");

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    public Connection getConnection(){
        return this.connection;
    }

    public void close(){
        try {
            this.connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}
