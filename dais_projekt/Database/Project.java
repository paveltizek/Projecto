package Database;

import Helpers.MyDate;
import Helpers.StringComparator;

import java.sql.*;
import java.util.ArrayList;


public class Project {
    private int id;
    private String name;
    private Date deadline;
    private float time;
    private Customer customer;
    private float cost;
    private String profitability;

    public Project() {
        this.id = 0;
    }

    public Project(int id) {

        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM PROJECT WHERE project_id=1");
            preparedStatement.clearParameters();
            //preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                this.id = resultSet.getInt("project_id");
                this.name = resultSet.getString("name");
                this.deadline = MyDate.toDate(resultSet.getString("deadline"));
                this.time = resultSet.getFloat("time");
                this.customer = new Customer(resultSet.getInt("customer_id"));
                this.cost = resultSet.getFloat("cost");
                this.profitability = resultSet.getString("profitability");
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
    }

    public Project(String name, Date deadline, float time, Customer customer, float cost, String profitability) {
        this.id = 0;
        this.name = name;
        this.deadline = deadline;
        this.time = time;
        this.customer = customer;
        this.cost = cost;
        this.profitability = profitability;
    }

    public Project(int id, String name, Date deadline, float time, Customer customer, float cost, String profitability) {
        this.id = id;
        this.name = name;
        this.deadline = deadline;
        this.time = time;
        this.customer = customer;
        this.cost = cost;
        this.profitability = profitability;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public void setTime(float time) {
        this.time = time;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public void setProfitability(String profitability) {
        this.profitability = profitability;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Date getDeadline() {
        return deadline;
    }

    public float getTime() {
        return time;
    }

    public float getCost() {
        return cost;
    }

    public String getProfitability() {
        return profitability;
    }

    public Customer getCustomer() {
        return customer;
    }

    public ArrayList<Project> all() {
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM PROJECT");
            preparedStatement.clearParameters();
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Project> projects = new ArrayList<>();
            while (resultSet.next()) {
                Project project = new Project();
                project.id = resultSet.getInt("project_id");
                project.name = resultSet.getString("name");
                project.deadline = MyDate.toDate(resultSet.getString("deadline"));
                project.time = resultSet.getFloat("time");
                project.customer = new Customer(resultSet.getInt("customer_id"));
                project.cost = resultSet.getFloat("cost");
                project.profitability = resultSet.getString("profitability");

                projects.add(project);
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
            return projects;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
        return null;
    }

    public boolean save() {
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement;
            if (this.id == 0) {
                preparedStatement = connection.prepareStatement("INSERT INTO PROJECT (NAME, DEADLINE, TIME, CUSTOMER_ID, COST, PROFITABILITY) VALUES (?, ?, ?, ?, ?, ?)");
                preparedStatement.clearParameters();
                preparedStatement.setString(1, this.name);
                preparedStatement.setDate(2, this.deadline);
                preparedStatement.setFloat(3, this.time);
                preparedStatement.setInt(4, this.customer.getId());
                preparedStatement.setFloat(5, this.cost);
                preparedStatement.setString(6, this.profitability);

                System.out.println(this);
            } else {
                preparedStatement = connection.prepareStatement("UPDATE PROJECT SET NAME=?, DEADLINE=?, TIME=?, CUSTOMER_ID=?, COST=?, PROFITABILITY=? WHERE PROJECT_ID=?");
                preparedStatement.clearParameters();
                preparedStatement.setString(1, this.name);
                preparedStatement.setDate(2, this.deadline);
                preparedStatement.setFloat(3, this.time);
                preparedStatement.setInt(4, this.customer.getId());
                preparedStatement.setFloat(5, this.cost);
                preparedStatement.setString(6, this.profitability);
                preparedStatement.setInt(7, this.id);
                System.out.println(this);
            }

            preparedStatement.execute();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        database.close();
        return true;
    }

    public ArrayList<Project> getByCustomer(Ticket ticket) {
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM PROJECT WHERE CUSTOMER_ID=?");
            preparedStatement.clearParameters();
            preparedStatement.setInt(1, ticket.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Project> projects = new ArrayList<>();
            while (resultSet.next()) {
                Project project = new Project();
                project.id = resultSet.getInt("project_id");
                project.name = resultSet.getString("name");
                project.deadline = MyDate.toDate(resultSet.getString("deadline"));
                project.time = resultSet.getFloat("time");
                project.customer = new Customer(resultSet.getInt("customer_id"));
                project.cost = resultSet.getFloat("cost");
                project.profitability = resultSet.getString("profitability");

                projects.add(project);
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
            return projects;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
        return null;
    }

    @Override
    public String toString() {
        return "Project{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", deadline=" + deadline +
                ", time=" + time +
                ", customer=" + customer +
                ", cost=" + cost +
                ", profitability='" + profitability + '\'' +
                '}';
    }

    public void changeProfitability(){
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            CallableStatement callableStatement = connection.prepareCall("{call changeProfitability(?)}");
            callableStatement.setInt(1, this.id);
            callableStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean isProfitable(){
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();
        float costs = 0;
        float price = 0;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT (p.COST) price, sum(tw.TIME*r.SALARY) costs \n" +
                    "FROM PROJECT p LEFT JOIN TICKET t ON p.PROJECT_ID=t.PROJECT_ID\n" +
                    "  LEFT JOIN TICKETWORK tw ON t.TICKET_ID=tw.TICKET_ID\n" +
                    "  LEFT JOIN EMPLOYEE e ON tw.EMPLOYEE_ID=e.EMPLOYEE_ID\n" +
                    "  LEFT JOIN ROLE r ON r.ROLE_ID = e.ROLE_ID\n" +
                    "WHERE p.PROJECT_ID=1\n" +
                    "GROUP BY p.COST");
            preparedStatement.clearParameters();
//            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                costs = resultSet.getFloat("costs");
                price = resultSet.getFloat("price");

            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
        return costs < price;
    }
}
