package Database;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by Pavel on 26.04.2015.
 */
public class Payout {
    private int id;
    private Date date;
    private float amount;
    private Date periodStart;
    private Date periodEnd;
    private float timeWorked;
    private Employee employee;

    public Payout() {
        this.id = 0;
    }

    public Payout(int id) {
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM PAYOUT WHERE PAYOUT_ID=?");
            preparedStatement.clearParameters();
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {

                this.id = resultSet.getInt("payout_id");
                this.date = resultSet.getDate("date");
                this.amount = resultSet.getFloat("amount");
                this.periodStart = resultSet.getDate("period_start");
                this.periodEnd = resultSet.getDate("period_end");
                this.timeWorked = resultSet.getFloat("time_worked");
                this.employee = new Employee(resultSet.getInt("employee_id"));



            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
    }

    public int getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public float getAmount() {
        return amount;
    }

    public Date getPeriodStart() {
        return periodStart;
    }

    public Date getPeriodEnd() {
        return periodEnd;
    }

    public float getTimeWorked() {
        return timeWorked;
    }

    public Employee getEmployee() {
        return employee;
    }

    public Payout(Date date, float amount, Date periodStart, Date periodEnd, float timeWorked, Employee employee) {
        this.id = 0;
        this.date = date;
        this.amount = amount;
        this.periodStart = periodStart;
        this.periodEnd = periodEnd;
        this.timeWorked = timeWorked;
        this.employee = employee;
    }

    public Payout(int id, Date date, float amount, Date periodStart, Date periodEnd, float timeWorked, Employee employee) {
        this.id = id;
        this.date = date;
        this.amount = amount;
        this.periodStart = periodStart;
        this.periodEnd = periodEnd;
        this.timeWorked = timeWorked;
        this.employee = employee;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public void setPeriodStart(Date period_start) {
        this.periodStart = period_start;
    }

    public void setPeriodEnd(Date period_end) {
        this.periodEnd = period_end;
    }

    public void setTimeWorked(float timeWorked) {
        this.timeWorked = timeWorked;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public ArrayList<Payout> all(){
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM PAYOUT");
            preparedStatement.clearParameters();
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Payout> payouts = new ArrayList<>();
            while (resultSet.next()) {
                Payout payout = new Payout();
                payout.id = resultSet.getInt("payout_id");
                payout.date = resultSet.getDate("date");
                payout.amount = resultSet.getFloat("amount");
                payout.periodStart = resultSet.getDate("period_start");
                payout.periodEnd = resultSet.getDate("period_end");
                payout.timeWorked = resultSet.getFloat("time_worked");
                payout.employee = new Employee(resultSet.getInt("employee_id"));
                payouts.add(payout);
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
            return payouts;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
        return null;
    }

    public ArrayList<Payout> getByEmployee(Employee employee){
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM PAYOUT WHERE EMPLOYEE_ID=?");
            preparedStatement.clearParameters();
            preparedStatement.setInt(1, employee.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Payout> payouts = new ArrayList<>();
            while (resultSet.next()) {
                Payout payout = new Payout();
                payout.id = resultSet.getInt("payout_id");
                payout.date = resultSet.getDate("date");
                payout.amount = resultSet.getFloat("amount");
                payout.periodStart = resultSet.getDate("period_start");
                payout.periodEnd = resultSet.getDate("period_end");
                payout.timeWorked = resultSet.getFloat("time_worked");
                payout.employee = new Employee(resultSet.getInt("employee_id"));
                payouts.add(payout);
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
            return payouts;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
        return null;
    }

    public ArrayList<Payout> getByEmployee(String lastName){
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM PAYOUT WHERE EMPLOYEE_ID=?");
            preparedStatement.clearParameters();
            preparedStatement.setString(1, "%" + lastName + "%");
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Payout> payouts = new ArrayList<>();
            while (resultSet.next()) {
                Payout payout = new Payout();
                payout.id = resultSet.getInt("payout_id");
                payout.date = resultSet.getDate("date");
                payout.amount = resultSet.getFloat("amount");
                payout.periodStart = resultSet.getDate("period_start");
                payout.periodEnd = resultSet.getDate("period_end");
                payout.timeWorked = resultSet.getFloat("time_worked");
                payout.employee = new Employee(resultSet.getInt("employee_id"));
                payouts.add(payout);
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
            return payouts;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
        return null;
    }

    public boolean save(){
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement;
            if (this.id == 0) {
                preparedStatement = connection.prepareStatement("INSERT INTO PAYOUT (date, AMOUNT, PERIOD_START, PERIOD_END, TIME_WORKED, EMPLOYEE_ID) VALUES (?, ?, ?, ?, ?, ?)");
                preparedStatement.clearParameters();
                preparedStatement.setDate(1, this.date);
                preparedStatement.setFloat(2, this.amount);
                preparedStatement.setDate(3, this.periodStart);
                preparedStatement.setDate(4, this.periodEnd);
                preparedStatement.setFloat(5, this.timeWorked);
                preparedStatement.setInt(6, this.employee.getId());
            } else {
                System.out.println("Update");
                preparedStatement = connection.prepareStatement("UPDATE PAYOUT SET date=?, AMOUNT=?, PERIOD_START=?, PERIOD_END=?, TIME_WORKED=?, EMPLOYEE_ID=? WHERE NOTIFICATION_ID=?");
                preparedStatement.clearParameters();
                preparedStatement.setDate(1, this.date);
                preparedStatement.setFloat(2, this.amount);
                preparedStatement.setDate(3, this.periodStart);
                preparedStatement.setDate(4, this.periodEnd);
                preparedStatement.setFloat(5, this.timeWorked);
                preparedStatement.setInt(6, this.employee.getId());
                preparedStatement.setInt(7, this.id);
            }

            preparedStatement.execute();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        database.close();
        return true;
    }

    @Override
    public String toString() {
        return "Payout{" +
                "id=" + id +
                ", date=" + date +
                ", amount=" + amount +
                ", periodStart=" + periodStart +
                ", periodEnd=" + periodEnd +
                ", timeWorked=" + timeWorked +
                ", employee=" + employee +
                '}';
    }
}
