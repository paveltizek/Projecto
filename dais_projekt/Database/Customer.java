package Database;

import Helpers.StringComparator;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Customer {
    private int id;
    private String companyName;
    private String secretaryName;
    private String email;
    private String phoneNumber;
    private Address address;

    public Customer() {
        this.id = 0;
    }

    public Customer(int id) {

        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM CUSTOMER WHERE customer_id=?");
            preparedStatement.clearParameters();
            preparedStatement.setString(1, String.valueOf(id));
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {

                this.id = resultSet.getInt("customer_id");
                this.companyName = resultSet.getString("company_name");
                this.secretaryName = resultSet.getString("secretary");
                this.email = resultSet.getString("email");
                this.phoneNumber = resultSet.getString("phone_number");
                this.address = new Address(resultSet.getInt("address_id"));


            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();

    }

    public Customer(String companyName, String secretaryName, String email, String phoneNumber, Address address) {
        this.id = 0;
        this.companyName = companyName;
        this.secretaryName = secretaryName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.address = address;
    }

    public Customer(int id, String companyName, String secretaryName, String email, String phoneNumber, Address address) {
        this.id = id;
        this.companyName = companyName;
        this.secretaryName = secretaryName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.address = address;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public void setSecretaryName(String secretaryName) {
        this.secretaryName = secretaryName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getSecretaryName() {
        return secretaryName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Address getAddress() {
        return address;
    }

    public ArrayList<Customer> all() {
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM ADDRESS");
            preparedStatement.clearParameters();
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Customer> customers = new ArrayList<>();
            while (resultSet.next()) {
                Customer customer = new Customer();
                customer.id = resultSet.getInt("customer_id");
                customer.companyName = resultSet.getString("company_name");
                customer.secretaryName = resultSet.getString("secretary");
                customer.email = resultSet.getString("email");
                customer.phoneNumber = resultSet.getString("phone_neumber");
                customer.address = new Address(resultSet.getInt("address_id"));
                customers.add(customer);
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
            return customers;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
        return null;
    }

    public boolean save() {
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement;
            if (this.id == 0) {
                preparedStatement = connection.prepareStatement("INSERT INTO CUSTOMER (COMPANY_NAME, SECRETARY, EMAIL, PHONE_NUMBER, ADDRESS_ID) VALUES (?, ?, ?, ?, ?)");
                preparedStatement.clearParameters();
                preparedStatement.setString(1, this.companyName);
                preparedStatement.setString(2, this.secretaryName);
                preparedStatement.setString(3, this.email);
                preparedStatement.setString(4, this.phoneNumber);
                preparedStatement.setInt(5, this.address.getId());
            } else {
                System.out.println("Update");
                preparedStatement = connection.prepareStatement("UPDATE CUSTOMER SET COMPANY_NAME=?, SECRETARY=?, EMAIL=?, PHONE_NUMBER=?, ADDRESS_ID=? WHERE CUSTOMER_ID=?");
                preparedStatement.clearParameters();
                preparedStatement.setString(1, this.companyName);
                preparedStatement.setString(2, this.secretaryName);
                preparedStatement.setString(3, this.email);
                preparedStatement.setString(4, this.phoneNumber);
                preparedStatement.setInt(5, this.address.getId());
                preparedStatement.setInt(6, this.id);
            }

            preparedStatement.execute();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        database.close();
        return true;
    }

    public ArrayList<Customer> getByAddress(Role role){
        Database database = new Database();
        database.open();
        Connection connection = database.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM ADDRESS WHERE ADDRESS_ID=?");
            preparedStatement.clearParameters();
            preparedStatement.setInt(1, role.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Customer> customers = new ArrayList<>();
            while (resultSet.next()) {
                Customer customer = new Customer();
                customer.id = resultSet.getInt("customer_id");
                customer.companyName = resultSet.getString("company_name");
                customer.secretaryName = resultSet.getString("secretary");
                customer.email = resultSet.getString("email");
                customer.phoneNumber = resultSet.getString("phone_neumber");
                customer.address = new Address(resultSet.getInt("address_id"));
                customers.add(customer);
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
            return customers;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();
        return null;
    }


    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", companyName='" + companyName + '\'' +
                ", secretaryName='" + secretaryName + '\'' +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", address=" + address +
                '}';
    }
}
