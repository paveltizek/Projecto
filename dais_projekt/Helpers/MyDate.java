package Helpers;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by Pavel on 25.04.2015.
 */
public class MyDate {
    public static java.sql.Date toDate (String date){
        try {
            return new java.sql.Date((new SimpleDateFormat("yyyy-MM-dd").parse(date)).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
