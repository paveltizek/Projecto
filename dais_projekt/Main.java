import Database.*;
import Helpers.MyDate;


import javax.swing.*;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by Pavel on 20.04.2015.
 */
public class Main {
    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        //Funkce 5.3
     /*
      Výplaty – po kliknutí na tlačítko bude vývojáři vyplacena odměna za kalendářním období a záznam se zapíše do tabulky Payout.
      Kontroluje se, jestli pro dané kalendářní období již nebyla vývojáři odměna vyplacena 5
      */
        new Employee(82).createPayout();

        //Nová pozice
        Role role = new Role();
        role.setSalary((float) 20.5);
        role.setRoleName("Printer");
        role.save();

        Address address = new Address(5);
        address.setCity("Olomouc");
        address.setCountry("CZ");
        address.setPostCode("85465");
        address.setStreet("Modrá 165/5");
        address.save();

        System.out.println(address);

        ArrayList<Address> addresses = new Address().all();
        System.out.println(addresses);

        //Nový zákazník
        Customer customer = new Customer();
        customer.setCompanyName("Google");
        customer.setEmail("google@email.cz");
        customer.setPhoneNumber("68468462");
        customer.setSecretaryName("Alena Voprasilova");
        customer.setAddress(new Address(3));
        System.out.println(customer);
        customer.save();


        //Nový zaměstnanec
        Employee employee = new Employee();
        employee.setEmail("pasc@asv.cz");
        employee.setFirstName("Karel");
        employee.setLastName("Novák");
        employee.setPassword("prdel");
        employee.setHireDate(MyDate.toDate("2014-03-17"));
        employee.setRole(new Role(3));
        employee.setAddress(new Address(3));
        employee.save();

        System.out.println(new Employee().all());

        //Nový projekt
        Project project = new Project();
        project.setCost((float) 20.5);
        project.setCustomer(new Customer(1));
        project.setDeadline(MyDate.toDate("2015-09-13"));
        project.setName("Trevor Phillips Inc.");
        project.save();
        System.out.println(new Project().all());

        //Nová faktura
        Invoice invoice = new Invoice();
        invoice.setCustomer(new Customer(1));
        invoice.setAmount((float) 5000);
        invoice.setPaid("No");
        invoice.setProject(new Project(1));
        invoice.save();

        //Funkce 5.4
        /*
         Vytvoření faktury – pro dokončený projekt (atribut status = „done“) zapsat fakturu do tabulky Invoice
         Volání při stisknutí tlačítka
         */
        Invoice.createInvoice(new Project(1), "Ne");


        //Nový projekt
        Project project2 = new Project("Toolbar", MyDate.toDate("2009-07-06"), 57, new Customer(8), 70000, "Yes");
        project2.save();

        //Funkce 5.5 - Kontrola výnosnosti projektu
        new Project(1).changeProfitability();

        //Funkce 6.1
        /*
        Tikety, na kterých vývojář pracoval
         */
        ArrayList<TicketWork> ticketWorks = new TicketWork().getByEmployee(new Employee(50));
        for (TicketWork ticketWork : ticketWorks) {
            System.out.println(ticketWork.getTicket());
        }

        //Funkce 6.2
        /*
        Odpracované doba zaměstnance
         */
        System.out.println(new Employee(63).getTimeWorked());

        //Funkce 6.3
        /*
         Náklady na projekt pro porovnání s rozpočtem projektu (ztrátový/výdělečný)
         */
        System.out.println(new Project(1).isProfitable());

        //Funkce 6.4
       /*
        Detail vývojáře – zobrazení všech informací daného vývojáře (jeho tikety, role, odměna, počet odpracovaných hodin)
        */
        Employee myEmployee = new Employee(11);

        System.out.println("Zaměstnanec: " + employee.getFirstName() + " " + employee.getLastName() + "\n" +
                "Pozice: " + employee.getRole().getRoleName() + " s hod. mzdou: " + employee.getRole().getSalary() + "\n");

        System.out.println("Pracoval na tiketech: ");
        float hours = 0;
        for (TicketWork ticketWork : ticketWorks) {

            hours += ticketWork.getTime();
            System.out.println(ticketWork.getTicket());
        }
        System.out.println("Celkem odpracoval: " + hours + " hodin.");


        System.out.println("Délka zpracování: " + String.valueOf(System.currentTimeMillis() - startTime) + "ms");

        //Kontrola, zda-li je projekt výnosný
        System.out.println(new Project(1).isProfitable());
    }
}
