
/*
5.1
*/
CREATE OR REPLACE PROCEDURE addTime(
  p_text TICKETWORK.TEXT%TYPE,
  p_time TICKETWORK.TIME%TYPE,
  p_employee_id TICKETWORK.EMPLOYEE_ID%TYPE,
  p_ticket_id TICKETWORK.TICKET_ID%TYPE)
AS
  v_ticket_time TICKET.TIME%TYPE;
  v_project_id TICKET.PROJECT_ID%TYPE;
  v_project_time PROJECT.TIME%TYPE;
  BEGIN
    INSERT INTO TICKETWORK (TEXT, TIME, EMPLOYEE_ID, TICKET_ID) VALUES (p_text, p_time, p_employee_id, p_ticket_id);

    SELECT TIME, PROJECT_ID INTO v_ticket_time, v_project_id FROM TICKET;
    v_ticket_time := v_ticket_time + p_time;
    UPDATE TICKET SET TIME=p_ticket_id WHERE TICKET_ID=p_ticket_id;

    SELECT TIME INTO v_project_time FROM PROJECT;
    v_project_time := v_project_time + p_time;
    UPDATE PROJECT SET TIME=v_project_time WHERE PROJECT_ID=v_project_id;
    COMMIT;
    EXCEPTION
      WHEN OTHERS THEN
        ROLLBACK;
  END addTime;

/*
5.2
*/
CREATE OR REPLACE
TRIGGER AddNotification
BEFORE INSERT ON TicketWork
FOR EACH ROW
  DECLARE
    CURSOR c_employees IS SELECT * FROM EMPLOYEE WHERE EMPLOYEE_ID IN (SELECT DISTINCT EMPLOYEE_ID FROM TICKETWORK WHERE TICKET_ID=:NEW.TICKET_ID);
    v_employee EMPLOYEE%ROWTYPE;
  BEGIN
    OPEN c_employees;
    LOOP
      FETCH c_employees INTO v_employee;
      EXIT WHEN c_employees%NOTFOUND;
      INSERT INTO NOTIFICATION (MESSAGE, EMPLOYEE_ID) VALUES (:NEW.TEXT, v_employee.EMPLOYEE_ID);
    END LOOP ;
    CLOSE c_employees;


  END;



/*
5.3
*/

CREATE OR REPLACE PROCEDURE createPayout(p_employee_id INTEGER)
AS
  v_amount FLOAT;
  v_employee_end_date EMPLOYEE.END_DATE%TYPE;
  v_period_start PAYOUT.PERIOD_START%TYPE;
  v_employee_id INTEGER;
  v_period_end PAYOUT.PERIOD_END%TYPE;
  v_time_worked FLOAT;
  v_temp_count INTEGER;

  BEGIN
    SELECT count(*) INTO v_temp_count FROM PAYOUT WHERE EMPLOYEE_ID=p_employee_id AND PERIOD_START=(TRUNC((SELECT ADD_MONTHS(sysdate,-1) FROM dual),'MON'));

    IF v_temp_count < 1 THEN
      SELECT (TRUNC((SELECT ADD_MONTHS(sysdate,-1) FROM dual),'MON')), (select last_day(to_date(add_months(sysdate, -1), 'dd-mm-yy')) from dual)
      INTO v_period_start, v_period_end FROM dual;
      SELECT END_DATE INTO v_employee_end_date FROM EMPLOYEE WHERE EMPLOYEE_ID=p_employee_id;
      IF v_employee_end_date > v_period_start  THEN
        SELECT SUM(tw.TIME), (SUM(tw.TIME)*r.SALARY), e.EMPLOYEE_ID INTO v_time_worked, v_amount, v_employee_id
        FROM ROLE r RIGHT JOIN EMPLOYEE e ON r.ROLE_ID = e.ROLE_ID
          LEFT JOIN TICKETWORK tw ON e.EMPLOYEE_ID = tw.EMPLOYEE_ID
        WHERE e.EMPLOYEE_ID=p_employee_id AND tw.timestamp BETWEEN v_period_start AND v_period_end
        GROUP BY r.SALARY, e.EMPLOYEE_ID;
        INSERT INTO PAYOUT ("date", AMOUNT, PERIOD_START, PERIOD_END, TIME_WORKED, EMPLOYEE_ID)
        VALUES (SYSDATE, v_amount, v_period_start, v_period_end, v_time_worked, p_employee_id);
        COMMIT ;

      END IF ;
    END IF ;

    EXCEPTION
    WHEN OTHERS THEN
    ROLLBACK ;
  END ;

/*
5.5
*/
CREATE OR REPLACE PROCEDURE changeProfitability(p_project_id PROJECT.PROJECT_ID%TYPE)
AS
  v_computed_cost FLOAT;
  v_cost PROJECT.COST%TYPE;
  BEGIN
    SELECT (p.COST), sum(tw.TIME*r.SALARY) cena INTO v_cost, v_computed_cost
    FROM PROJECT p LEFT JOIN TICKET t ON p.PROJECT_ID=t.PROJECT_ID
      LEFT JOIN TICKETWORK tw ON t.TICKET_ID=tw.TICKET_ID
      LEFT JOIN EMPLOYEE e ON tw.EMPLOYEE_ID=e.EMPLOYEE_ID
      LEFT JOIN ROLE r ON r.ROLE_ID = e.ROLE_ID
    WHERE p.PROJECT_ID = p_project_id
    GROUP BY p.COST;
    IF v_computed_cost < v_cost THEN
      UPDATE PROJECT SET PROFITABILITY='Ano' WHERE PROJECT_ID=p_project_id;
    ELSE
      UPDATE PROJECT SET PROFITABILITY='Ne' WHERE PROJECT_ID=p_project_id;
    END IF;
    COMMIT;
    EXCEPTION
      WHEN OTHERS THEN
        ROLLBACK;
  END;




